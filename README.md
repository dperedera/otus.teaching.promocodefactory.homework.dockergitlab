# Otus.Teaching.PromoCodeFactory

Проект для домашних заданий и демо по курсу `C# ASP.NET Core Разработчик` от `Отус`.
Cистема `Promocode Factory` для выдачи промокодов партнеров для клиентов по группам предпочтений.

Данный проект является стартовой точкой для Homework №3

Подробное описание проекта можно найти в [Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Home)

Описание домашнего задания в [Homework Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Homework-3)

Домашнее задание поможет отработать несколько навыков:

Перевод приложения на другую БД, что поможет увидеть преимущества использования ORM
Настройка хостинга в Docker и docker-compose для запуска внешней инфраструктуры нужной приложению, в качестве инфраструктуры будет выступать БД.
Настройки CI со сборкой на GitLab, упаковкой в Docker образ и передачей в хранилище GitLab, что позволит легко развернуть приложение в любом окружении


# Описание
Сделать форк репозитория [Homework 3](https://gitlab.com/devgrav/otus.teaching.promocodefactory.homework.dockergitlab) домашнего задания и реализовать пункты в нем

1. Добавляем docker-compose файл, где конфигурируем PostgreSQL БД.
1. Добавляем начальную миграцию БД
1. Вместо SQLite добавляем PostgreSQL в приложение и настраиваем EF на работу с PostgreSQL, проверяем через Swagger, что Read/Create/Update методы работают
1. Настраиваем сборку на Gitlab



# Критерий оценивания

- Пункты 1-3: по 2 балла.
- Пункт 4: 3 балла.
- Минимальный проходной порог: 6 баллов.
